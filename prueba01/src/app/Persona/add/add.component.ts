import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Persona } from 'src/app/Modelo/Persona';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  formUser : FormGroup

  constructor(private router: Router, private service: ServiceService) {
      this.formUser = new FormGroup({
        mail          : new FormControl({value: ''}),
        estilo_musica : new FormControl({value: ''}),
      })
   }

  ngOnInit(): void {
    this.formUser.controls["mail"].setValue("");
    this.formUser.controls["estilo_musica"].setValue("");
  }

  Guardar(){

    let persona = new Persona();
    persona.mail=this.formUser.controls["mail"].value;
    persona.estilo_musica=this.formUser.controls["estilo_musica"].value;

    if(persona.mail != "" || persona.estilo_musica!=""){
        this.service.createPersona(persona)
        .subscribe(data=>{
        alert("Se Agregó con Exito...!!!");
        this.router.navigate(["listar"])
      })
    }
    else{
      alert("Llene ambos campos para continuar");
    }
  }

}
